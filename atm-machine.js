let atmMachine;

const setState = newState => {
    atmMachine = Object.create(newState);    
}

const NoCard = {
    removeCard: function() {
        console.log('No card to remove');
        setState(HasCard);
    },
    insertCard: function() {
        console.log('Inserting card...');
        setState(HasCard);
    }
}

const HasCard = {
    insertCard: function() {
        console.log('Card already inserted!');
    },
    removeCard: function() {
        console.log('Removing card...');
        setState(NoCard);
    }
}

setState(NoCard);

let atmMachine;

const setState = newState => {
    atmMachine = Object.create(newState);    
}

const NoCard = {
    removeCard: function() {
        console.log('No card to remove');
        setState(HasCard);
    },
    insertCard: function() {
        console.log('Inserting card...');
        setState(HasCard);
    }
}

const HasCard = {
    insertCard: function() {
        console.log('Card already inserted!');
    },
    removeCard: function() {
        console.log('Removing card...');
        setState(NoCard);
    }
}

setState(NoCard);

atmMachine.insertCard();
atmMachine.removeCard();